const trainer = {

    name : 'Ash Ketchum',
    age : 10,
    pokemon : ["Pickachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends : {
        hoenn : ["May", "Max"],
        kanto : ["Brock", "Misty"]
    },

    //talk method
    talk : function(){
        //choose on pokemon array
        console.log(`${trainer.pokemon[0]} ! I choose you`);
    }
   
};

trainer.talk();


//contructor function
function Pokemon(name, lvl){
    this.name = name;
    this.lvl = lvl;
    this.hp = this.lvl * 3;
    this.atk = this.lvl * 1.5;

    //teckle method
    this.tackle = function(pokemon){
        console.log(`${this.name} tackled ${pokemon.name}`)
    }

    //faint method
    this.faint = function(){
        console.log(`${this.name} has fainted`);
    }



    

 
}


//create an instance from the pokemon constructor function
let p1 = new Pokemon("Raichu", 12);
let p2 = new Pokemon("Mr. Mime", 8);
let p3 = new Pokemon("Ditto", 100);

p1.tackle(p2);
p2.faint();